#ifndef BUBBLESORT_HPP_INCLUDED
#define BUBBLESORT_HPP_INCLUDED

#include <vector>

namespace Algo
{
    namespace BubbleSort
    {
        /*
        Sort input element using Bubble Sort Algorithm
        Input : array of elements
        Output : Sorted Elements
        */
        void sort(std::vector<int>& p_input);

        /*
        Print Elements
        */
        void print(std::vector<int>& p_data);
    }
}

#endif // BUBBLESORT_HPP_INCLUDED
