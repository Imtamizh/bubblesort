#include <iostream>
#include "BubbleSort.hpp"

using namespace Algo::BubbleSort;

/*
Test code for Bubble Sort
*/
int main()
{
    std::cout << "#### Bubble Sort Example ###" << std::endl;

    std::vector<int> input;
    int n;
    int element;

    std::cout << " Enter the number of Elements:" ;
    std::cin >> n;

    std::cout << "Enter the Elements:" ;

    for(int i = 0; i < n; i++)
    {
        std::cin >> element;
        input.push_back(element);
    }

    std::cout << "Invoking Sort....." << std::endl;

    sort(input);

    std::cout << "Sorted Elements are...." << std::endl;
    print(input);

    return 0;
}

