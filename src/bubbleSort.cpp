#include <iostream>
#include "BubbleSort.hpp"

/*
Print Elements
*/
void Algo::BubbleSort::print(std::vector<int>& p_data)
{
    for(unsigned int i = 0; i < p_data.size(); i++)
    {
        std::cout << p_data[i] << std::endl;
    }
}

/*
Sort input element using Bubble Sort Algorithm
Input : array of elements
Output : Sorted Elements
*/
void Algo::BubbleSort::sort(std::vector<int>& p_input)
{
    int n = p_input.size();

    // n passes to bubble up the biggest number to last in the array.
    for(int i = 0; i < (n - 1); i++)
    {
        //For Each pass skip last 'i' elements since already it would be in sorted format.
        for(int j = 0; j < ((n-1) - i); j++)
        {
            if(p_input[j] > p_input[j+1])
            {
                //Swap the 2 elements
                p_input[j]    ^= p_input[j+1];
                p_input[j+1]  ^= p_input[j];
                p_input[j]    ^= p_input[j+1];
            }
        }
    }
}
